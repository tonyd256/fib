module Compute.Fibonacci
    ( fib
    ) where

-- Reference of optimized implementations
-- https://wiki.haskell.org/The_Fibonacci_sequence

fib :: Int -> Int
fib x = let initial = [0, 1] in
    last $ take (x + 1) $ initial ++ calc initial
  where
    calc xs = let next = sum xs in
        next : calc [last xs, next]

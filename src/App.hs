module App
    ( app
    ) where

import Compute.Fibonacci
import Compute.Prime

app :: Int -> String
app n
    | n < 0 = "No solution for n < 0."
    | n == 0 = "0" -- Guard before the Modulo cases
    | isFibonacciPrime n = "BuzzFizz"
    | n `rem` 20 == 0 = "FizzBuzz" -- Modulo 15 has a 0 cycle every 20
    | n `rem` 5 == 0 = "Fizz" -- Modulo 5 has a 0 cycle every 5
    | n `rem` 4 == 0 = "Buzz" -- Modulo 3 has a 0 cycle every 4
    | otherwise = show $ fib n

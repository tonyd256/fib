# fib
Fibonacci-Prime coding exercise.

## Setup

Install `stack` from [https://docs.haskellstack.org/en/stable/README/#how-to-install]()

Then run the setup script to install the dependencies:

```
bin/setup
```

## Usage

To run the program, run `stack exec fib` in the terminal.

## Tests

To run the tests, run `stack test` in the terminal.

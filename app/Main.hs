module Main where

import System.IO (hSetBuffering, stdout, BufferMode(NoBuffering))

import App
import Helpers

main :: IO ()
main = do
    hSetBuffering stdout NoBuffering

    putStrLn "Fibonacci Calculator"
    putStr "Enter a number: "
    putStrLn =<< maybe "NaN" app <$> (readMaybe <$> getLine)

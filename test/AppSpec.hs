module AppSpec
    ( main
    , spec
    ) where

import Test.Hspec

import App

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "app" $ do
        it "returns FizzBuzz when given a multiple of 20" $ do
            app 40 `shouldBe` "FizzBuzz"
        it "returns Fizz when given a multiple of 5" $ do
            app 25 `shouldBe` "Fizz"
        it "returns Buzz when given a multiple of 4" $ do
            app 28 `shouldBe` "Buzz"

module HelpersSpec
    ( main
    , spec
    ) where

import Test.Hspec

import Helpers

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "readMaybe" $ do
        it "returns an Int when given a valid string" $ do
            (readMaybe "45" :: Maybe Int) `shouldBe` Just 45
        it "returns Nothing when given an invalid string" $ do
            (readMaybe "s4df5" :: Maybe Int) `shouldBe` Nothing

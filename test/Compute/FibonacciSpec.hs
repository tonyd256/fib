module Compute.FibonacciSpec
    ( main
    , spec
    ) where

import Test.Hspec

import Compute.Fibonacci

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "fib" $ do
        it "returns 0 when given 0" $ do
            fib 0 `shouldBe` 0
        it "returns 1 when given 1" $ do
            fib 1 `shouldBe` 1
        it "returns 1 when given 2" $ do
            fib 2 `shouldBe` 1
